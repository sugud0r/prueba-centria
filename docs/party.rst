 .. _users:

Party ticket reservation
======================================================================

This is a very simple ticket reservation with not payment gateway implemented.
Ticekts are classified to "Bronze", "Silver" and "Gold" types. A customer can only
buy only one ticket per buy. A "guest list" is created for each part containing the
customer and adquired ticket type. The "Party" model contain detailed information
about the scheduled party.

.. automodule:: triacen.party.models
   :members:
   :noindex:
