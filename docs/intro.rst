Proyecto de admición
======================================================================

Mi idea simple fue realizar un "organizador" de eventos; fiestas, en este caso.
Para ello cree tres modelos (Party, Ticket, Guest) y aproveché el módelo User.
Podrán haber tres tipos de tickets diferentes (GLD, SIL, BRZ) que pueden ser adquiridos
en cualquier fiesta. Los tickets podrán ser creados en el admin, así como las fiestas.

Las fiestas es el único modelo que se muestra en el front, permitiendo reservar un ticket
si eres un usuario normal, o mostrar todos los tickets que dicha fiesta tiene reservados
en caso de que seas superusuario. Los demás modelos pueden ser manejados desde el admin.

El endpoint para GraphQL se encuentra en /graphql
