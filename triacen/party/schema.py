# Copyright 2021 Moisés González.
# Use of this source code is governed by The Unlicense
# which can be found in the LICENSE file.

from django.forms import fields
import graphene
from graphene_django import DjangoObjectType
from graphene_django.forms.mutation import DjangoModelFormMutation

from triacen.party.models import *
from triacen.party.forms import TicketForm


class TicketType(DjangoObjectType):
    class Meta:
        model = Ticket


class PartyType(DjangoObjectType):
    class Meta:
        model = Party
        fields = "__all__"


class GuestType(DjangoObjectType):
    class Meta:
        model = Guest
        fields = "__all__"


class TicketMutation(DjangoModelFormMutation):
    class Meta:
        form_class = TicketForm


class Mutation(graphene.ObjectType):
    update_ticket = TicketMutation.Field()


class Query(graphene.ObjectType):
    all_tickets = graphene.List(TicketType)
    all_parties = graphene.List(PartyType)
    all_guests = graphene.List(GuestType)

    def resolve_all_guests(root, info):
        return Guest.objects.all()

    def resolve_all_paries(root, info):
        return Party.objects.all()

    def resolve_all_tickets(root, info):
        return Ticket.objects.all()


schema = graphene.Schema(query=Query, mutation=Mutation)
