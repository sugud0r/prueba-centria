from django.contrib import admin

from triacen.party.models import *

admin.site.register(
    [
        Party,
        Ticket,
        Guest,
    ]
)
