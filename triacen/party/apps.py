from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "triacen.party"
    verbose_name = _("Party")

    def ready(self):
        try:
            import triacen.party.signals  # noqa F401
        except ImportError:
            pass
