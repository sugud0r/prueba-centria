from django.urls import path

from triacen.party.views import *

app_name = 'party'

urlpatterns = [
    path("all/", view=PartyListView.as_view(), name="list"),
    path("info/<slug:slug>/", view=PartyDetailView.as_view(), name="detail"),
    path("update/<slug:slug>/", view=PartyUpdateView.as_view(), name="update"),
    path("delete/<slug:slug>/", view=PartyDeleteView.as_view(), name="delete"),
    path("new/", view=PartyCreateView.as_view(), name="new")
]
