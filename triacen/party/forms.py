from django import forms
from django import forms
from django.db.models import fields
from triacen.party.models import *


class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = "__all__"


class PartyAddForm(forms.ModelForm):
    class Meta:
        model = Party
        fields = ['name', 'country', 'city', 'date', 'max_persons']


class PartyBuyTicketForm(forms.ModelForm):
    class Meta:
        model = Guest
        fields = ['ticket']
