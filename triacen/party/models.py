from django.db import models
from django.utils.text import slugify
from django.urls import reverse
from django.conf import settings


class Ticket(models.Model):
    BRONZE = 'BRZ'
    SILVER = 'SIL'
    GOLD = 'GLD'

    TICKET_TYPES = [
        (BRONZE, 'Bronze'),
        (SILVER, 'Silver'),
        (GOLD, 'Gold'),
    ]

    type = models.CharField(
        max_length=3,
        choices=TICKET_TYPES,
    )
    price = models.DecimalField(
        decimal_places=2,
        max_digits=5,
    )

    def __str__(self) -> str:
        return self.type


class Party(models.Model):
    name = models.CharField(max_length=128, null=False, blank=False)
    country = models.CharField(max_length=128, null=False, blank=False)
    city = models.CharField(max_length=128, null=False, blank=False)
    date = models.DateTimeField(
        null=False,
        blank=False,
    )
    max_persons = models.PositiveIntegerField(
        null=False,
        blank=False,
    )
    slug = models.SlugField(null=False, blank=False, unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)


    def get_absolute_url(self):
        return reverse('party:detail', kwargs={'slug': self.slug})


    def __str__(self) -> str:
        return self.name


class Guest(models.Model):
    party = models.ForeignKey(Party, on_delete=models.CASCADE)
    customer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)


    def __str__(self) -> str:
        return f'{self.customer} on {self.party} with {self.ticket}'
