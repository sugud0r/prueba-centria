from django.urls.base import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth import get_user_model

from triacen.party.models import Party, Guest
from triacen.party.forms import PartyAddForm, PartyBuyTicketForm


class PartyListView(ListView):

    model = Party
    paginate_by = 20


class PartyDetailView(DetailView):

    model = Party
    form = PartyBuyTicketForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated:
            context['form'] = self.form()

        if self.request.user.is_superuser:
            tickets = Guest.objects.filter(party_id=self.get_object().id)
            context['tickets'] = tickets

        return context

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)
        party = self.get_object()

        if form.is_valid():
            User = get_user_model()

            guest = form.save(commit=False)
            guest.customer = User.objects.get(id=request.user.id)
            guest.party = party
            guest.save()

            messages.add_message(request, messages.SUCCESS, f'Your {guest.ticket} ticket to {party} is reserved!')

        return HttpResponseRedirect(reverse('party:detail', kwargs={'slug': party.slug}))


class SlugSuccessURLMixing():

    def get_success_url(self):
        return reverse('party:detail', kwargs={'slug': self.object.slug})


class OnlySuperUserMixin(UserPassesTestMixin):

    def test_func(self):
        return self.request.user.is_superuser


class BasePartyEditViews():

    model = Party
    form_class = PartyAddForm


class PartyCreateView(LoginRequiredMixin, OnlySuperUserMixin, CreateView, SlugSuccessURLMixing):

    model = BasePartyEditViews.model
    form_class = BasePartyEditViews.form_class


class PartyUpdateView(LoginRequiredMixin, OnlySuperUserMixin, SuccessMessageMixin, UpdateView, SlugSuccessURLMixing):

    model = BasePartyEditViews.model
    form_class = BasePartyEditViews.form_class
    success_message = "Information successfully updated"


class PartyDeleteView(LoginRequiredMixin, OnlySuperUserMixin, DeleteView):

    model = Party
    success_url = reverse_lazy('party:all')
